var annotated_dup =
[
    [ "controller", null, [
      [ "Controller", "classcontroller_1_1Controller.html", "classcontroller_1_1Controller" ]
    ] ],
    [ "encoderDriver", null, [
      [ "Encoder", "classencoderDriver_1_1Encoder.html", "classencoderDriver_1_1Encoder" ]
    ] ],
    [ "FrontEnd", null, [
      [ "UI", "classFrontEnd_1_1UI.html", "classFrontEnd_1_1UI" ]
    ] ],
    [ "lab3", null, [
      [ "simonSays", "classlab3_1_1simonSays.html", "classlab3_1_1simonSays" ]
    ] ],
    [ "me405_lab4", null, [
      [ "MCP9808", "classme405__lab4_1_1MCP9808.html", "classme405__lab4_1_1MCP9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "Motor", "classmotorDriver_1_1Motor.html", "classmotorDriver_1_1Motor" ]
    ] ],
    [ "nucleoSerial", null, [
      [ "nucleoSerial", "classnucleoSerial_1_1nucleoSerial.html", "classnucleoSerial_1_1nucleoSerial" ]
    ] ],
    [ "pid", null, [
      [ "ClosedLoop", "classpid_1_1ClosedLoop.html", "classpid_1_1ClosedLoop" ]
    ] ]
];