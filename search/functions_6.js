var searchData=
[
  ['gendata_205',['genData',['../classnucleoSerial_1_1nucleoSerial.html#a593239864774ef622e6255f3b6637418',1,'nucleoSerial::nucleoSerial']]],
  ['generatelevel_206',['generateLevel',['../classlab3_1_1simonSays.html#ad38c0329ad5da33ba6c29e4f4a9ecef1',1,'lab3::simonSays']]],
  ['get_5fdegrees_207',['get_degrees',['../classencoderDriver_1_1Encoder.html#a96fbd51159bda6064881efea22a8442a',1,'encoderDriver::Encoder']]],
  ['get_5fdelta_208',['get_delta',['../classencoderDriver_1_1Encoder.html#aa361876954dabe72496d11593eac85f6',1,'encoderDriver::Encoder']]],
  ['get_5fj_209',['get_J',['../classpid_1_1ClosedLoop.html#aecb50ac76d9b8b4872d99dd32b000ff9',1,'pid::ClosedLoop']]],
  ['get_5fkd_210',['get_Kd',['../classpid_1_1ClosedLoop.html#a6a95cb5e6f83916c5e7638bf08421773',1,'pid::ClosedLoop']]],
  ['get_5fki_211',['get_Ki',['../classpid_1_1ClosedLoop.html#afe7f1d59b08265d6f48eafe8de8e1741',1,'pid::ClosedLoop']]],
  ['get_5fkp_212',['get_Kp',['../classpid_1_1ClosedLoop.html#a9f5e6299d76e94ad102b63ae7d8bb5ca',1,'pid::ClosedLoop']]],
  ['get_5fposition_213',['get_position',['../classencoderDriver_1_1Encoder.html#ad0e4e375973b3248724ff975f6c5448b',1,'encoderDriver::Encoder']]],
  ['get_5fradians_214',['get_radians',['../classencoderDriver_1_1Encoder.html#a4f751b522da20da8a6d02761e0a19acc',1,'encoderDriver::Encoder']]],
  ['getchange_215',['getChange',['../vendotron_8py.html#a23bd645e85b4a183244f0e011f4450f3',1,'vendotron']]],
  ['getmotorstate_216',['getMotorState',['../shares_8py.html#a5866154c44674fe32e45cd5c5a9be8f2',1,'shares']]],
  ['getnextreferenceposition_217',['getNextReferencePosition',['../classcontroller_1_1Controller.html#acc323dc2c6bef1b2cea0b4e1d51772bc',1,'controller::Controller']]]
];
