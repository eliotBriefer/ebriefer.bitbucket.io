var searchData=
[
  ['sawtoothwave_231',['sawtoothWave',['../lab2_8py.html#afcc0cf4c0c3b505cf83759daa6b2d4e7',1,'lab2']]],
  ['searchinterp_232',['searchInterp',['../csvResampler_8py.html#a22697fea07ffbead6efcfb8c6c0eb375',1,'csvResampler']]],
  ['selftune_233',['selfTune',['../classcontroller_1_1Controller.html#a62e278d1254cbf56971664ef8d955698',1,'controller::Controller']]],
  ['send_234',['send',['../me405__lab3_8py.html#a7fc9ecb6d9f476f2bfa855c97756f076',1,'me405_lab3']]],
  ['sendchar_235',['sendChar',['../classFrontEnd_1_1UI.html#ad596f53ab28204778af31255839d7703',1,'FrontEnd.UI.sendChar()'],['../me405__lab3__front__end_8py.html#afd6b5a2a8db7e261fa4c7bace317fd13',1,'me405_lab3_front_end.sendChar()']]],
  ['senddata_236',['sendData',['../classnucleoSerial_1_1nucleoSerial.html#a4151d3833f876572ff2030c29fadc4e2',1,'nucleoSerial::nucleoSerial']]],
  ['set_5fduty_237',['set_duty',['../classmotorDriver_1_1Motor.html#a7458ccd805e2d6b176b6cd91f0dd2572',1,'motorDriver::Motor']]],
  ['set_5fkd_238',['set_Kd',['../classpid_1_1ClosedLoop.html#accd37f6c97eb4f396ce90f24bd702fcb',1,'pid::ClosedLoop']]],
  ['set_5fki_239',['set_Ki',['../classpid_1_1ClosedLoop.html#a26f4be9819a8a34ef530ae2e9edfdd07',1,'pid::ClosedLoop']]],
  ['set_5fkp_240',['set_Kp',['../classpid_1_1ClosedLoop.html#a2bc32e4bc7619428ba6fb758789150cd',1,'pid::ClosedLoop']]],
  ['set_5fposition_241',['set_position',['../classencoderDriver_1_1Encoder.html#ac3e4a2833b0a2855e7671a056354bfb6',1,'encoderDriver::Encoder']]],
  ['sinewave_242',['sineWave',['../lab2_8py.html#a3499186b7bcce430cfd6008a7382ac9b',1,'lab2']]],
  ['squarewave_243',['squareWave',['../lab2_8py.html#ae1df3edf31a903cbd41698513e30b1d3',1,'lab2']]],
  ['step_244',['step',['../classcontroller_1_1Controller.html#af2a900bf2d3efbe11ace4a3b18a4a7cb',1,'controller::Controller']]]
];
