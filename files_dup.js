var files_dup =
[
    [ "controller.py", "controller_8py.html", "controller_8py" ],
    [ "csvResampler.py", "csvResampler_8py.html", "csvResampler_8py" ],
    [ "encoderDriver.py", "encoderDriver_8py.html", "encoderDriver_8py" ],
    [ "HW2.py", "HW2_8py.html", "HW2_8py" ],
    [ "Lab1.py", "Lab1_8py.html", "Lab1_8py" ],
    [ "lab2.py", "lab2_8py.html", "lab2_8py" ],
    [ "lab3.py", "lab3_8py.html", "lab3_8py" ],
    [ "me405_lab3.py", "me405__lab3_8py.html", "me405__lab3_8py" ],
    [ "me405_lab3_front_end.py", "me405__lab3__front__end_8py.html", "me405__lab3__front__end_8py" ],
    [ "me405_lab4.py", "me405__lab4_8py.html", "me405__lab4_8py" ],
    [ "motorDriver.py", "motorDriver_8py.html", "motorDriver_8py" ],
    [ "nucleoMain.py", "nucleoMain_8py.html", "nucleoMain_8py" ],
    [ "nucleoSerial.py", "nucleoSerial_8py.html", [
      [ "nucleoSerial", "classnucleoSerial_1_1nucleoSerial.html", "classnucleoSerial_1_1nucleoSerial" ]
    ] ],
    [ "pid.py", "pid_8py.html", [
      [ "ClosedLoop", "classpid_1_1ClosedLoop.html", "classpid_1_1ClosedLoop" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "vendotron.py", "vendotron_8py.html", "vendotron_8py" ]
];